import { Typography, Space } from 'antd';
import styles from './card.module.css'

export function Card({item , active, setFilteredItem, setActive}) {
  const { Text } = Typography;

  // const [activeCard, setActiveCard] = useState(active)
  
  // const handleState=()=>{
  //   setActiveCard(active)
 

  // }

  return (
    <>
    <div 
       className={active === `${item.name}` ? `${styles.card}` : `${styles.item}`}
         onClick={function () {
          setFilteredItem(item.category);
           setActive(item.name);
         }}>
      <img src={item.icon} alt="" className={styles.imgPic} />
      <Text type="secondary" className={styles.cardText}>
        {item.name}
      </Text>
    </div>
  </>
  );
}
export default Card;
