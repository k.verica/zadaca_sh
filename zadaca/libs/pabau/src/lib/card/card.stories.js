import Card from './card';
import Icon from '../../../../../apps/skinhealth/src/app/Assets/acne.svg'

export default {
  component: Card,
  title: 'Card',
  args :{
  item:{
    name: 'Verica',
    icon : Icon,
  },
},

};

const Template = (args) => <Card {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  
};
