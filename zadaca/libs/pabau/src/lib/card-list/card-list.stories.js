import CardList from './card-list';

export default {
  component: CardList,
  title: 'CardList',
};

const Template = (args) => <CardList {...args} />;

export const Primary = Template.bind({});
Primary.args = {};
