import './button.module.css';
import styles from './button.module.css'


export function Button({label, variant,  backgroundColor }) {

  const style = {
    backgroundColor, 
    padding:'10px 20px',
    border: '1px solid black'
  }
  return (
    <button style={style} >
      {label}
    </button>
  );
}
export default Button;
