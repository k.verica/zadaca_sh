import Pabau from './pabau';

export default {
  component: Pabau,
  title: 'Pabau',
};

const Template = (args) => <Pabau {...args} />;

export const Primary = Template.bind({});
Primary.args = {};
