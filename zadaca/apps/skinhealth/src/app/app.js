import styles from './app.module.css';
import Navbar from './Components/Navbar/Navbar';

// import {Card} from '@zadaca/pabau'
import 'antd/dist/antd.css';
import CardList from './Components/CardList/CardList';
import { defaultItems }  from './data'
export function App() {
  return (
    <>
    <Navbar/>
     <div className={styles.Content}>
    <CardList  defaultItems={defaultItems}/>
     </div>
     
 </>
  );
}
export default App;
