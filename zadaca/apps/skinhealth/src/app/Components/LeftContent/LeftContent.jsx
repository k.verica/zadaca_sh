import React, { useState, useEffect } from "react";
import "./LeftContent.css";
import RightContent from "../RightContent/RightContent";
import { Row, Col } from "antd";

export default function LeftContent({ filteredItem }) {
  const [subFilter, setSubFilter] = useState([]);
  const [active, setActive] = useState("small_boxes");

  function handleSubFilters(current) {
    const array = [];
    filteredItem.map((item) => {
      if (current === item.name) {
        item.subCategory.map((x) => {
          array.push({
            key: x.key,
            name: x.name,
            review: x.review,
            price: x.price,
            time: x.time,
            rating: x.rating,
          });
        });
      }
    });
    console.log(array);
    setSubFilter(array);
  }
  return (
    <>
      <Row gutter={[16, 16]} className="Content_main">
        <Col span={6}>
          <div className="leftContent_root">
            {filteredItem.map((item, idx) => (
              <div
                key={idx}
                className="small_boxes"
                onClick={() => handleSubFilters(item.name)}
              >
                <h4 className="textSpan">
                  {item.name}
                  <span className="numSpan">{item.rdmValue}</span>
                </h4>
              </div>
            ))}
          </div>
        </Col>

        <Col span={18}>
          <div className="rightContent_root">
            <RightContent subFilter={subFilter} />
          </div>
        </Col>
      </Row>
    </>
  );
}
