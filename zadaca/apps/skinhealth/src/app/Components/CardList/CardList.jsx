import React, { useState, useEffect } from 'react';
import './CardList.css';
import { Row, Col } from 'antd';
import './CardList.css';
import { Typography, Space } from 'antd';
import { MenuOutlined, IdcardOutlined } from '@ant-design/icons';
import LeftContent from '../LeftContent/LeftContent';
import { Card } from '@zadaca/pabau';
import { Button } from '@zadaca/pabau';


export default function CardList({ defaultItems }) {
  const { Text, Link } = Typography;

  //state
  const [active, setActive] = useState('Botox');
  const [filteredItem, setFilteredItem] = useState([]);
  const [list, setList] = useState([])

  useEffect(()=>{
    handeleAll()
  })

  const handeleAll =() => {
   const arr =[]
   defaultItems.map(item => {
     return(
       item.category.map(x=> arr.push({
         name:x.name,
         rdmValue:x.rdmValue
       }))
     )
   })
   setList(arr)
   setActive()
  }
  return (
    <>
  
      <Row className="cardList">
        <div className="cardItem"
        onClick={handeleAll}>
          <MenuOutlined className="imgPic" />
          <Text className="cardText" type="secondary">
            All
          </Text>
        </div>

        {defaultItems.map((item) => {
          return (
          <Card item={item} active={active} setFilteredItem={setFilteredItem} setActive={setActive} 
         
          ></Card>
          );
        })}

        {console.log(filteredItem)}
        <div className="cardItem">
          <IdcardOutlined className="imgPic" />
          <Text className="cardText" type="secondary">
            Voucher
          </Text>
        </div>
      </Row>
    
      <LeftContent filteredItem={filteredItem} />
    </>
  );
}
