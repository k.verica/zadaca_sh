import React from "react";
import "./RightContent.css";
import { Row, Col } from "antd";
import { Typography, Space } from "antd";

export default function RightContent({ subFilter }) {
  const { Text, Link } = Typography;

  return (
    <>
      <Row className="rightContainer">
        <Col span={12} className="textBtn">
          <Text type="secondary">In Clinic</Text>
        </Col>
        <Col span={12} className="textBtn">
          <Text type="secondary">Virtual Consultation</Text>
        </Col>

        {subFilter.map((item) => (
          <div className="boxContent">
            <h4 className="subName">{item.name}</h4>

            <p className="subTime">{item.time} min</p>

            <div>${item.price}</div>
          </div>
        ))}
      </Row>
    </>
  );
}
