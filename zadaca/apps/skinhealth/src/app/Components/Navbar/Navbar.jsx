import React from "react";
import "./Navbar.css";
import { Row, Col } from "antd";
import { Button } from "antd";
import logo from "../../Images/logo.png";
import { Typography } from "antd";
// import { Button } from "@zadaca/pabau";

export default function Navbar() {
  const { Text, Link } = Typography;
  const { Title } = Typography;

  return (
    <>
      <Row
        gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
        className="Navbar_container"
      >
        <Col span={6}>
          <img src={logo} alt="logo" className="logo_img" />
        </Col>
        <Col span={18} className="textNav">
          <Title level={5}>Choose Service</Title>
          <Text type="secondary">Step 1/8</Text>
        </Col>
        {/* <Button  label='Menu Btn' backgroundColor='red'/> */}
      </Row>
    </>
  );
}
